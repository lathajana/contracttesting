﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ContactTesting.Tests.MiddleWare
{
    public class ProviderStateMiddleware
    {
        private const string ConsumerName = "Service_Consumer";//{ConsumerNmae"}
        private readonly RequestDelegate _next;
        private readonly IDictionary<string, Action> _providerStates;
        private int i = 0;
        private dynamic contractFile;

        public ProviderStateMiddleware(RequestDelegate next)
        {
            _next = next;
            _providerStates = new Dictionary<string, Action>
            {
                { "StateName",UnAssignPackage{Method name"} },
                { "Invoking assign permission to a package",UnAssignPermission },
                { "Invoking assign permission for a package which is already assigned",AssignPermission },
                { "Invoking create site_licenses",DummyMethod },
                { "Invoking GetPackages",DummyMethod },
                { "Invoking assign package to site license which is already assigned",AssignPackage },
                { "Invoking create permissions for existing permission",CreatePermission },
                { "Invoking create site_license code",DummyMethod },
                { "Invoking create permissions",DeletePermissionAsync },
                { "Invoking ActivateSiteLicense",DummyMethod },
                { "Invoking UpdateSiteLicense",DummyMethod },
                { "Invoking create packages",DeletePackage },
                { "Invoking create packages for existing package",CreatePackage },
                { "Invoking EnableSiteLicenseCode",DummyMethod },
                { "Invoking DisableSiteLicenseCode",DummyMethod },
                { "Invoking DeActivateSiteLicense",DummyMethod },
                { "Invoking update a package", DummyMethod },
                { "Invoking remove package from site license", AssignPackageForRemoveRequest },
                { "Invoking Returns a purchase",DummyMethod },
                { "Invoking Suspend User Account", DummyMethod },
                { "Invoking Reinstate a User Account", DummyMethod },
                { "Invoking List all the site license codes and their usage based on a query", DummyMethod },
                { "Generate  Users",DummyMethod },
                { "Prepare a merge between two user accounts",DummyMethod },
                { "Retrieve a list of users based on a query", DummyMethod},
                { "Commit Merge",DummyMethod },
                { "Update  Users",DummyMethod },
                { "Invoking Validate The Address Fields", DummyMethod },
                { "Invoking Validate The Address Fields where Streeet Address Is Null", DummyMethod },
                { "Invoking Validate The Address Fields where address is not deliverable", DummyMethod },
                {"Invoking remove package from site license which is already removed", UnAssignPackageForRemoveRequest }

            };
        }
        private void DummyMethod()
        {
            //Blank method for provider state for no tasks
        }
        private void CreatePermission()
        {
            HttpClient _client = new HttpClient();
            ModelName body = new ModelName();
            body.permissions = new List<PermissionRequest>();
            PermissionRequest PermissionRequestBody = new PermissionRequest();
            PermissionRequestBody.permission_type = "LAzXrTegtt";
            PermissionRequestBody.name = "LAzXrTegtt";
            body.permissions.Add(PermissionRequestBody);
            var json = JsonConvert.SerializeObject(body);
            var data = new StringContent(json, Encoding.UTF8, "application/json");
            var url = "Actual Url";
            _client.DefaultRequestHeaders.Add("Authorization", "");
            _client.DefaultRequestHeaders.Add("Accept", "application/json");
            _ = _client.PostAsync(url, data);
        }
        private void DeletePermissionAsync()
        {
            HttpClient _client = new HttpClient();
            _client.BaseAddress = new Uri("url");
            var request = new HttpRequestMessage(HttpMethod.Get, "path");
            request.Headers.Add("Accept", "application/json");
            request.Headers.Add("Authorization", "T");
            var response = _client.SendAsync(request);
            var content = JsonConvert.DeserializeObject<GetAllPermissionsDto>(response.Result.Content.ReadAsStringAsync().Result);
            var exist = false;
            var permId = 0;

            for (int i = 0; i < content.data.Count; i++)
            {
                if (content.data[i].name == "6rWljKQjUu" && content.data[i].permission_type == "6rWljKQjUu")
                {
                    permId = content.data[i].id;
                    exist = true;
                    break;
                }
            }
            if (exist)
            {

                var requestDelete = new HttpRequestMessage(HttpMethod.Delete, "path" + permId.ToString());
                requestDelete.Headers.Add("Accept", "application/json");
                requestDelete.Headers.Add("Authorization", "Token token=6139888a-e60b-4fef-900d-7530fd430e04");
                _ = _client.SendAsync(requestDelete);
            }
        }
        private void UnAssignPermission()
        {
            HttpClient _client = new HttpClient();
            _client.BaseAddress = new Uri("url should be passed");
            var request = new HttpRequestMessage(HttpMethod.Delete, "/employee/1");
            request.Headers.Add("Accept", "application/json");
            request.Headers.Add("Authorization", "");
            _ = _client.SendAsync(request);
        }
        private void AssignPermission()
        {
            AssignPermissionsDto body = new AssignPermissionsDto();
            body.permissions = new List<AssignPermissions>();
            AssignPermissions AssignPermissionsRequestBody = new AssignPermissions();
            AssignPermissionsRequestBody.id = 2;
            body.permissions.Add(AssignPermissionsRequestBody);
            var json = JsonConvert.SerializeObject(body);
            var data = new StringContent(json, Encoding.UTF8, "application/json");
            var url = "https://qa2-auth.nasm.org/api/v3/packages/100/permissions";
            HttpClient _client = new HttpClient();
            _client.DefaultRequestHeaders.Add("Authorization", "Token token=6139888a-e60b-4fef-900d-7530fd430e04");
            _client.DefaultRequestHeaders.Add("Accept", "application/json");
            _ = _client.PostAsync(url, data);
        }
        private void UnAssignPackage()
        {
            HttpClient _client = new HttpClient();
            _client.BaseAddress = new Uri("https://qa2-auth.nasm.org");
            var request = new HttpRequestMessage(HttpMethod.Delete, "/api/v3/site_licenses/1/packages/101");
            request.Headers.Add("Accept", "application/json");
            request.Headers.Add("Authorization", "Token token=6139888a-e60b-4fef-900d-7530fd430e04");
            _ = _client.SendAsync(request);
        }


        private void AssignPackage()
        {
            HttpClient _client = new HttpClient();
            var body = new AssignPackagetoSiteLicenseBody();
            body.packages = new List<PackageIdBody>();
            PackageIdBody myPackage = new PackageIdBody();
            myPackage.package_id = 100;
            body.packages.Add(myPackage);
            var json = JsonConvert.SerializeObject(body);
            var data = new StringContent(json, Encoding.UTF8, "application/json");
            var url = "";
            _client.DefaultRequestHeaders.Add("Authorization", "");
            _client.DefaultRequestHeaders.Add("Accept", "application/json");
            _ = _client.PostAsync(url, data);
        }

        private void UnAssignPackageForRemoveRequest()
        {
            HttpClient _client = new HttpClient();
            _client.BaseAddress = new Uri("");
            var request = new HttpRequestMessage(HttpMethod.Delete, "");
            request.Headers.Add("Accept", "application/json");
            request.Headers.Add("Authorization", "");
            _ = _client.SendAsync(request);
        }
        private void AssignPackageForRemoveRequest()
        {
            HttpClient _client = new HttpClient();
            var body = new AssignPackagetoSiteLicenseBody();
            body.packages = new List<PackageIdBody>();
            PackageIdBody myPackage = new PackageIdBody();
            myPackage.package_id = 102;
            body.packages.Add(myPackage);
            var json = JsonConvert.SerializeObject(body);
            var data = new StringContent(json, Encoding.UTF8, "application/json");
            var url = "";
            _client.DefaultRequestHeaders.Add("Authorization", "");
            _client.DefaultRequestHeaders.Add("Accept", "application/json");
            _ = _client.PostAsync(url, data);
        }
        private async void DeletePackage()
        {
            HttpClient _client = new HttpClient();
            _client.BaseAddress = new Uri("");
            var request = new HttpRequestMessage(HttpMethod.Get, "/employee/1");
            request.Headers.Add("Accept", "application/json");
            request.Headers.Add("Authorization", "");
            var response = _client.SendAsync(request);
            var content = JsonConvert.DeserializeObject<PackageDto>(response.Result.Content.ReadAsStringAsync().Result);
            var exist = false;
            var packId = 0;

            for (int i = 0; i < content.data.Count; i++)
            {
                if (content.data[i].name == "NcCNiXqYPq")
                {
                    packId = content.data[i].id;
                    exist = true;
                    break;
                }
            }
            if (exist)
            {

                var requestDelete = new HttpRequestMessage(HttpMethod.Delete, "/employee/1" + packId.ToString());
                requestDelete.Headers.Add("Accept", "application/json");
                requestDelete.Headers.Add("Authorization", "T");
                _ = await _client.SendAsync(requestDelete);
            }
        }
        private void CreatePackage()
        {
            HttpClient _client = new HttpClient();
            CreatePackages body = new CreatePackages();
            body.name = "WTxNrVqYQp";
            body.id = 12345;
            body.created_at = "";
            body.updated_at = "";
            body.deleted_at = "";
            var json = JsonConvert.SerializeObject(body);
            var data = new StringContent(json, Encoding.UTF8, "application/json");
            var url = "";
            _client.DefaultRequestHeaders.Add("Authorization", "");
            _client.DefaultRequestHeaders.Add("Accept", "application/json");
            _ = _client.PostAsync(url, data);
        }

        public async Task Invoke(HttpContext context)
        {

            if (context.Request.Path.Value == "/provider-states")
            {
                this.HandleProviderStatesRequest(context);
                await context.Response.WriteAsync(String.Empty);

            }
            else
            {
                await this._next(context);
            }
        }

        private void HandleProviderStatesRequest(HttpContext context)
        {
            context.Response.StatusCode = (int)HttpStatusCode.OK;
            string jsonRequestBody = String.Empty;
            //StreamReader reader = new StreamReader(@"/builds/", Encoding.UTF8);
            StreamReader reader = new StreamReader(@"path of json file", Encoding.UTF8);
            jsonRequestBody = reader.ReadToEnd();

            contractFile = JValue.Parse(jsonRequestBody.ToString());
            while (i < contractFile.interactions.Count)
            {
                _providerStates[contractFile.interactions[i].providerState.Value].Invoke();
                i++;

            }

        }
    }

}
}
